package com.applydigital.service;

import com.applydigital.model.Users;

public interface IUserService {
	
	int save(Users u) throws Exception;

}
