package com.applydigital.adapter.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.applydigital.adapter.IHackerNewsAdapter;
import com.applydigital.mappers.StoriesMapper;
import com.applydigital.request.HackerNewsRequest;
import com.applydigital.response.Hit;
import com.applydigital.response.Response;
import com.applydigital.service.IStoriesServices;

@Service
public class HackerNewsAdapter implements IHackerNewsAdapter {

	@Value("${url_service}")
	private String url_hacker_news;

	@Autowired
	private IStoriesServices service;

	@Autowired
	private StoriesMapper mapper;

	@Override
	public Response get(String searchValue) throws Exception {

		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<Response> response = restTemplate.getForEntity(url_hacker_news + searchValue,
					Response.class);

			return response.getBody();
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public int import_hacker_news(HackerNewsRequest req) throws Exception {
		try {
			Response response = get(req.getSearch_value());

			for (Hit h : response.getHits()) {
				service.save(mapper.toStories(h));
			}

			return 200;
		} catch (Exception e) {
			throw e;
		}
	}
}