package com.applydigital.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.applydigital.model.Tags;

public interface ITagsServices {
	
	Page<Tags> getByTags(String tags, Pageable pageable) throws Exception;

}