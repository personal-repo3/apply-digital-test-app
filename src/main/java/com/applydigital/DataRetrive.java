package com.applydigital;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;


import com.applydigital.adapter.IHackerNewsAdapter;

import com.applydigital.request.HackerNewsRequest;


@Configuration
@EnableScheduling
public class DataRetrive {

	@Autowired
	IHackerNewsAdapter adapter;

	@Value("${default_value_search}")
	private String default_search;




	@Scheduled(fixedDelayString = "${time}")
	public void scheduledDataRetrive() throws Exception {
		adapter.import_hacker_news(new HackerNewsRequest(default_search));
	}

}
