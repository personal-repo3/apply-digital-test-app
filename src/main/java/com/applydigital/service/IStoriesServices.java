package com.applydigital.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


import com.applydigital.model.Stories;
import com.applydigital.service.common.IService;

public interface IStoriesServices extends IService<Stories, Integer> {
	
	Stories getByStoryId(Integer story_id);

	Page<Stories> getByAuthor(String author, Pageable pageable) throws Exception;

	Page<Stories> getByTitle(String title, Pageable pageable) throws Exception;

	Page<Stories> getByCreateAtMonth(String month, Pageable pageable) throws Exception;
	
	Page<Stories> getByTag(List<String>tags, Pageable pageable);
}