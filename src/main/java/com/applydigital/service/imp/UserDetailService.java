package com.applydigital.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.applydigital.repository.IUsersRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service("userDetailsService")
public class UserDetailService implements UserDetailsService {

	Logger logger = LoggerFactory.getLogger(UserDetailService.class);

	@Autowired
	private IUsersRepository repo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return repo.findOneByUsername(username);
	}
}