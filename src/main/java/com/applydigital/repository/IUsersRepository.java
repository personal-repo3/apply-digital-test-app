package com.applydigital.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.applydigital.model.Users;

@Repository
@Transactional
public interface IUsersRepository extends JpaRepository<Users, String> {

	List<Users> findByEnabled(boolean enabled);

	@Transactional
	Users findOneByUsername(String username);

	@Query("FROM Users us where us.username = :user")
	Users checkByUserName(@Param("user") String user) throws Exception;

	@Transactional
	@Modifying
	@Query("UPDATE Users us SET us.password = :password WHERE us.username = :name")
	void changePassword(@Param("password") String clave, @Param("name") String name) throws Exception;
	
}