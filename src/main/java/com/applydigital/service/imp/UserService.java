package com.applydigital.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.applydigital.model.Users;
import com.applydigital.repository.IUsersRepository;
import com.applydigital.service.IUserService;

@Service
public class UserService implements IUserService {

	@Autowired
	private IUsersRepository repo;

	@Override
	public int save(Users u) throws Exception {
		try {
			repo.save(u);
			return 200;
		} catch (Exception e) {
			throw e;
		}
	}
}
