package com.applydigital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.applydigital.adapter.IHackerNewsAdapter;
import com.applydigital.request.HackerNewsRequest;
import com.applydigital.response.ErrorResponse;
import com.applydigital.response.Response;

@RestController
@RequestMapping("/api/v1/hacking-news")
public class RetrievingController {

	@Autowired
	IHackerNewsAdapter adapter;

	@GetMapping(value = "/{serch_value}")
	public ResponseEntity<?> get(@PathVariable("serch_value") String search_value) {
		try {

			return new ResponseEntity<Response>(adapter.get(search_value), HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error when consulting the service",
					e.getMessage(), "com.applydigital.controller.RetrievingController"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/import")
	public ResponseEntity<?> import_hacker_news(@RequestBody HackerNewsRequest req) {
		try {
			return new ResponseEntity<Integer>(adapter.import_hacker_news(req), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error when consulting the service",
					e.getMessage(), "com.applydigital.controller.RetrievingController"),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}