# apply-digital-test-app
This application is created for solicitude of Apply Digital for demostrate my domain in software development using Spring and other tools.
## Getting started

After clone the project.

1. Enter with the command line and compile the project using mvn install -DskipTests
2. After the Creation of Jar file, enter docker-compose up for creating the database and run the back end.

Automaticly the back end call the service described in the document for pupulate the database for first time, after that is gonna runing after 10 minuts

The Documentation of the service are described in this link

http://localhost:8080/swagger-ui/index.html

## Token Authentication
After start runing, the back end create an user for tokenize the rest services. The credentials and the access are

Method Type: Post
Structured: Multipart Form
url: http://localhost:8080/oauth/token
grant_type: password
user: apply_digital
password: Pa$$w0rd
**Auth: Basic **
username: applydigital
password: applydigital

## Application Properties
In case to be necesary you can change some properties like the default search, the url of rest service for retrive the data, the user and the password of basic auth for the tokenize service.
