package com.applydigital.response;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Hit implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LocalDateTime created_at;
    private String title;
    private String url;
    private String author;
    private Integer points;
    private String story_text;
    private String comment_text;
    private Integer num_comments;
    private Integer story_id;
    private String story_title;
    private String story_url;
    private Integer parent_id;
    private Integer created_at_i;
    private ArrayList<String> _tags;
    private String objectID;
    private HighlightResult _highlightResult;
}
