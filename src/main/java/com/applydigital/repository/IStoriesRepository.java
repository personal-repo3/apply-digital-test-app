package com.applydigital.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.applydigital.model.Stories;

@Repository
public interface IStoriesRepository extends JpaRepository<Stories, Integer> {
	
	Optional<Stories> findByStoryId(Integer storyId);
	
	Page<Stories> findByAuthor(String author, Pageable pageable);

	Page<Stories> findByTitle(String title, Pageable pageable);

	@Query(value = "select *  from stories s where trim(TO_CHAR(s.created_at , 'Month'))=:month", nativeQuery = true)
	Page<Stories> findByCreateAtMonth(@Param("month")String month, Pageable pageable);
	
	@Query(value = "select t.story  from Tags t where t.tag in(:tags)")
	Page<Stories> findByTag(@Param("tags")List<String> tags, Pageable pageable);
	

}