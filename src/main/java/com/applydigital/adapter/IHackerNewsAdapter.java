package com.applydigital.adapter;

import com.applydigital.request.HackerNewsRequest;
import com.applydigital.response.Response;

public interface IHackerNewsAdapter {
	
	Response get(String searchValue)throws Exception;
	
	int import_hacker_news(HackerNewsRequest req) throws Exception;

}
