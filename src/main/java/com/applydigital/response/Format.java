package com.applydigital.response;

import java.io.Serializable;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Format implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int highlighting;
    private int total;
}
