package com.applydigital.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.applydigital.model.Tags;
import com.applydigital.repository.ITagsRepository;
import com.applydigital.service.ITagsServices;

@Service
public class TagsService implements ITagsServices {

	@Autowired
	ITagsRepository repo;

	@Override
	public Page<Tags> getByTags(String tags, Pageable pageable) throws Exception {
		try {
			return repo.findByTag(tags, pageable);
		} catch (Exception e) {
			throw e;
		}
	}
}