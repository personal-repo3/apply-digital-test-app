package com.applydigital.util;

import org.springframework.stereotype.Component;

@Component
public class Utils {

	public String capital_letter(String value) {
		return value.substring(0, 1).toUpperCase() + value.substring(1);
	}

}
