package com.applydigital.response;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Title implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String value;
    private String matchLevel;
    private boolean fullyHighlighted;
    private ArrayList<String> matchedWords;

}
