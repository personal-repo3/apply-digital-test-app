package com.applydigital.mappers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.applydigital.model.Stories;
import com.applydigital.model.Tags;
import com.applydigital.response.Hit;

@Component
public class StoriesMapper {

	public Stories toStories(Hit hit) {
		Stories story = new Stories();
		story.setCreated_at(hit.getCreated_at());
		story.setTitle(hit.getTitle());
		story.setUrl(hit.getUrl());
		story.setAuthor(hit.getAuthor());
		story.setPoints(hit.getPoints());
		story.setStory_text(hit.getStory_text());
		story.setComment_text(hit.getComment_text());
		story.setNum_comments(hit.getNum_comments());
		story.setStoryId(hit.getStory_id());
		story.setStory_title(hit.getStory_title());
		story.setStory_url(hit.getStory_url());
		story.setParent_id(hit.getParent_id());
		story.setCreated_at_i(hit.getCreated_at_i());
		story.setObjectID(hit.getObjectID());

		story.setTags(tags(story));

		return story;
	}

	private List<Tags> tags(Stories story) {

		List<Tags> t = new ArrayList<Tags>();
		t.add(new Tags(story, "comment", ""));
		t.add(new Tags(story, "author", story.getAuthor()));
		t.add(new Tags(story, "story", story.getObjectID()));

		return t;
	}
}