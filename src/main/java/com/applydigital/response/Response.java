package com.applydigital.response;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Response implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Hit> hits;
    private Integer nbHits;
    private Integer page;
    private Integer nbPages;
    private Integer hitsPerPage;
    private boolean exhaustiveNbHits;
    private boolean exhaustiveTypo;
    private Exhaustive exhaustive;
    private String query;
    private String params;
    private Integer processingTimeMS;
    private ProcessingTimingsMS processingTimingsMS;
    
}
