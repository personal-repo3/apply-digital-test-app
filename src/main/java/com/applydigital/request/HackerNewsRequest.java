package com.applydigital.request;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HackerNewsRequest implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String search_value;
}