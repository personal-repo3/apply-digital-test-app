package com.applydigital.service.common;

public interface IService<T, ID> {
	
	int save(T t) throws Exception;
	
	T get(ID id) throws Exception;

}