package com.applydigital.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.applydigital.model.Tags;

@Repository
public interface ITagsRepository extends JpaRepository<Tags, Integer> {

	Page<Tags> findByTag(String tags, Pageable pageable);
}
