package com.applydigital.service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.applydigital.model.Stories;
import com.applydigital.repository.IStoriesRepository;
import com.applydigital.service.IStoriesServices;
import com.applydigital.util.Utils;

@Service
public class StoriesService implements IStoriesServices {

	@Autowired
	private IStoriesRepository repo;

	@Autowired
	private Utils util;

	@Override
	public int save(Stories t) throws Exception {
		try {
			repo.saveAndFlush(t);
			return 200;
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Stories get(Integer id) throws Exception {
		try {
			return repo.findById(id).orElse(null);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Page<Stories> getByAuthor(String author, Pageable pageable) throws Exception {
		try {
			return repo.findByAuthor(author, pageable);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Page<Stories> getByTitle(String title, Pageable pageable) throws Exception {
		try {
			return repo.findByTitle(title, pageable);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Page<Stories> getByCreateAtMonth(String month, Pageable pageable) throws Exception {
		try {
			return repo.findByCreateAtMonth(util.capital_letter(month), pageable);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public Stories getByStoryId(Integer story_id) {
		try {
			return repo.findByStoryId(story_id).orElse(null);
		} catch (Exception e) {
			throw e;
		}

	}

	@Override
	public Page<Stories> getByTag(List<String> tags, Pageable pageable) {
		try {
			return repo.findByTag(tags, pageable);
		} catch (Exception e) {
			throw e;
		}
	}
}