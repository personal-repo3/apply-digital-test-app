package com.applydigital.response;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PagginationResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<T> list;
	private Integer currentPage;
	private Long totalItems;
	private Integer totalPages;

}
