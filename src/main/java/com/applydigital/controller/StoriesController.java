package com.applydigital.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.applydigital.model.Stories;
import com.applydigital.response.ErrorResponse;
import com.applydigital.response.PagginationResponse;
import com.applydigital.service.IStoriesServices;

@RestController
@RequestMapping("/api/v1/stories")
public class StoriesController {

	@Autowired
	private IStoriesServices service;

	@GetMapping(value = "/author/{author}")
	public ResponseEntity<?> getByAuthor(@PathVariable("author") String author,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {

		try {
			Pageable paging = PageRequest.of(page, size);
			Page<Stories> stories = service.getByAuthor(author, paging);

			return new ResponseEntity<PagginationResponse<Stories>>(
					new PagginationResponse<Stories>(stories.getContent(), stories.getNumber(),
							stories.getTotalElements(), stories.getTotalPages()),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error Getting Stories by Author", e.getMessage(),
					"com.applydigital.controller.StoriesController"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/tittle/{tittle}")
	public ResponseEntity<?> getByTittle(@PathVariable("tittle") String tittle,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {

		try {
			Pageable paging = PageRequest.of(page, size);
			Page<Stories> stories = service.getByTitle(tittle, paging);

			return new ResponseEntity<PagginationResponse<Stories>>(
					new PagginationResponse<Stories>(stories.getContent(), stories.getNumber(),
							stories.getTotalElements(), stories.getTotalPages()),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error Getting Stories by Tittle", e.getMessage(),
					"com.applydigital.controller.StoriesController"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/create-at-month-name/{month-name}")
	public ResponseEntity<?> getByCreateAtMonthName(@PathVariable("month-name") String monthName,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {

		try {
			Pageable paging = PageRequest.of(page, size);
			Page<Stories> stories = service.getByCreateAtMonth(monthName, paging);

			return new ResponseEntity<PagginationResponse<Stories>>(
					new PagginationResponse<Stories>(stories.getContent(), stories.getNumber(),
							stories.getTotalElements(), stories.getTotalPages()),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error Getting Stories by Create at Month Name", e.getMessage(),
					"com.applydigital.controller.StoriesController"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value = "/tags/{tags}")
	public ResponseEntity<?> getByCreateAtMonthName(@PathVariable("tags") List<String> tags,
			@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size) {

		try {
			Pageable paging = PageRequest.of(page, size);
			Page<Stories> stories = service.getByTag(tags, paging);

			return new ResponseEntity<PagginationResponse<Stories>>(
					new PagginationResponse<Stories>(stories.getContent(), stories.getNumber(),
							stories.getTotalElements(), stories.getTotalPages()),
					HttpStatus.OK);

		} catch (Exception e) {
			return new ResponseEntity<ErrorResponse>(new ErrorResponse(500, "Error Getting Stories by Tags", e.getMessage(),
					"com.applydigital.controller.StoriesController"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}