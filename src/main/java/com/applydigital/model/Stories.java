package com.applydigital.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(schema = "public", name = "stories")
public class Stories implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@JsonProperty("story_id")
	@Column(name = "story_id")
	private Integer storyId;

	@Column
	private Integer parent_id;

	@Column
	private Integer num_comments;

	@Column
	private Integer points;

	@Column(length = 1000)
	private String title;

	@Column(length = 1000)
	private String url;

	@Column(length = 200)
	private String author;

	@Column(length = 200)
	private String story_text;

	@Column(length = 40000)
	private String comment_text;

	@Column(length = 1000)
	private String story_title;

	@Column(length = 1000)
	private String story_url;

	@Column
	private Integer created_at_i;

	@Column
	private String objectID;

	@Column
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime created_at;
	
	@OneToMany(mappedBy = "story", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Tags>tags;

}